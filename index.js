const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const sqlDbFactory = require("knex");
const process = require("process");

let sqlDb;

function initSqlDB() {
  /* Locally we should launch the app with TEST=true to use SQLlite:

       > TEST=true node ./index.js

    */
  if(false) {
    sqlDb = sqlDbFactory({
      client: "sqlite3",
      debug: true,
      connection: {
        filename: "./other/test.sqlite"
      },
      useNullAsDefault: true
    });
  } else {
    sqlDb = sqlDbFactory({
      debug: true,
      client: "pg",
      connection: process.env.DATABASE_URL,
      ssl: true
    });
  }
}

function initDb(name, table,data) {
  return sqlDb.schema.hasTable(name).then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable(name, table)
        .then(() => {
          return Promise.all(
            _.map(data, p => {
             
              return sqlDb(name).insert(p);
            })
          );
        });
    } else {
      return true;
    }
  });
}

const _ = require("lodash");

let serverPort = process.env.PORT || 5000;


app.use(express.static(__dirname + "/public"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// /* Register REST entry point */
function entryget(rest,table, pkey) {
    app.get(rest, (req,res)=>{
            let myQuery =sqlDb(table).where(pkey, req.params.name) ;//在table中去找name=rest地址冒号的参数的那个条目
        myQuery.then(function(result){
            console.log(result);
            res.send(JSON.stringify(result));
        });
            });
}

entryget("/staff/:staffid","staff","staffid");
entryget("/service/:serviceid","service","serviceid");
entryget("/location/:locationid","location","locationid");
entryget("/available/:locationid","available","locationid");
entryget("/operation/:serviceid","operation","serviceid");



app.get("/staff", function(req, res) {
      let myQuery = sqlDb("staff");
      myQuery.then(result => {
          res.send(JSON.stringify(result));
        });
    });
app.get("/service", function(req,res){
            let myQuery =sqlDb("service");
                        myQuery.then(result=>{
                            res.send(JSON.stringify(result));
                        });
            });
app.get("/location", function(req,res){
            let myQuery =sqlDb("location");
                        myQuery.then(result=>{
                            res.send(JSON.stringify(result));
                        });
            });
app.get("/available", function(req,res){
            let myQuery =sqlDb("available");
                        myQuery.then(result=>{
                            res.send(JSON.stringify(result));
                        });
            });
app.get("/operation", function(req,res){
            let myQuery =sqlDb("operation");
                        myQuery.then(result=>{
                            res.send(JSON.stringify(result));
                        });
            });

initSqlDB();
initDb("location", 
       table => {
          
          table.string("locationid");
          table.string("name");
          table.string("information");
          table.string("address");
          table.string("contact");
          table.string("photo");
        },
      require("./other/location.json")
);
initDb("service", 
       table => {
          table.string("serviceid");
          table.string("name");
          table.string("introduction");
                table.string("photo");
        },
      require("./other/service.json")
);
initDb("staff", 
       table => {
          table.string("staffid");
          table.string("position");
          table.string("name");
        table.string("photo");
    table.string("email");
    table.string("brief");
          table.string("introduction");
    
        },
      require("./other/staff.json")
);
initDb("operation", 
       table => {
          table.string("serviceid");
          table.string("staffid");
        },
      require("./other/operation.json")
);
initDb("available", 
       table => {
          table.string("serviceid");
          table.string("locationid");
        },
      require("./other/available.json")
);


//app.get("/pets", function(req, res) {
//  let start = parseInt(_.get(req, "query.start", 0));
//  let limit = parseInt(_.get(req, "query.limit", 5));
//  let sortby = _.get(req, "query.sort", "none");
//  let myQuery = sqlDb("pets");
//
//  if (sortby === "age") {
//    myQuery = myQuery.orderBy("born", "asc");
//  } else if (sortby === "-age") {
//    myQuery = myQuery.orderBy("born", "desc");
//  }
//  myQuery
//    .limit(limit)
//    .offset(start)
//    .then(result => {
//      res.send(JSON.stringify(result));
//    });
//});

//app.delete("/pets/:id", function(req, res) {
//  let idn = parseInt(req.params.id);
//  sqlDb("pets")
//    .where("id", idn)
//    .del()
//    .then(() => {
//      res.status(200);
//      res.send({ message: "ok" });
//    });
//});
//
//app.post("/pets", function(req, res) {
//  let toappend = {
//    name: req.body.name,
//    tag: req.body.tag,
//    born: req.body.born
//  };
//  sqlDb("pets")
//    .insert(toappend)
//    .then(ids => {
//      let id = ids[0];
//      res.send(_.merge({ id, toappend }));
//    });
//});

// app.use(function(req, res) {
//   res.status(400);
//   res.send({ error: "400", title: "404: File Not Found" });
// });

app.set("port", serverPort);


/* Start the server on port 3000 */
app.listen(serverPort, function() {
  console.log("success");
});
