# README
* Heroku URL:https://polimi-hyp-2018-team-10624971.herokuapp.com/index.html
* Bitbucket repo URL: https://bitbucket.org/dashboard/overview
* Team administrator: Xuanying, Cheng, 10624971, polimi-hyp-2018-10624971
# Description of the REST API
/staff/:staffid
/service/:serviceid
/location/:locationid
/available/:locationid
/operation/:serviceid  (id is the identifier of the subject in the database)
/staff
/service
/location
/available
/operation
# General description of the project
The project is a website for a carecenter provides the following content:
all services and all services by locations
all locations
all working staff
The working staff who operates the specific service
The service is provided by which locations and operated by whom
The location provides which services
# Division of work
Work is done alone
# Client-side language used: 
JAVASCRIPT, HTML, CSS, JQUERY
# Template used
http://www.cssmoban.com/cssthemes/7126.shtml
# main problem meet
can't well realize the order of names with backend.