$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '../staff',
        datatype: 'json',
        success: function (data) {

            var pointer = $('.service-grid').children();
            $.each(JSON.parse(data), function (i, item) {
                pointer.find('.img-responsive').attr("src", item.photo);
                pointer.find('.name').text(item.name);
                pointer.find('.position').text(item.position);
                pointer.find('.staffintroduction').text(item.brief);

                pointer.find('.address').attr("href", "staff1.html?id=" + item.staffid);
                pointer = pointer.next();
                console.log(item);

            });
        },
        error: function (data) {
            alert(typeof data);

        }
    });
});
