$(document).ready(function () {
    var pageid = GetURLParameter('id');



    function GetURLParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }


    $.ajax({
        type: 'GET',
        url: '../service',
        datatype: 'json',
        success: function (data) {
            $("#snumber").text(JSON.parse(data)[pageid].name);
            $("#stitle").text(JSON.parse(data)[pageid].name);
            $("#sp").text(JSON.parse(data)[parseInt(pageid)].introduction);
            $("#spic").attr("src", JSON.parse(data)[pageid].photo);
            //js for service in locations
            $("#servicenum").text(JSON.parse(data)[pageid].name);
            $("#servicenum").attr("href", "service1.html?id=" + pageid);
            $("#sitem").text(JSON.parse(data)[pageid].name + " locations");
            $("#sername").text(JSON.parse(data)[pageid].name);
            //js for service's staff
            $("#ser_name").text(JSON.parse(data)[pageid].name);
            $("#ser_name").attr("href", "service1.html?id=" + pageid);
            $("#ser_title").text(JSON.parse(data)[pageid].name + " staffs");
            $("#sertitle").text(JSON.parse(data)[pageid].name);






            setNext();
            setPrevious();
        },
        error: function (data) {
            alert(data);
        }

    });
    setNext = function () {
        var TheDemoURL = window.location.protocol + '//' + window.location.host,
            urlid;
        urlid = parseInt(pageid) + 1;
        if (urlid > 5)
            urlid = 0;
        $("#next").attr("href", TheDemoURL + '/pages/service1.html?id=' + urlid)
    };
    setPrevious = function () {
        var TheDemoURL = window.location.protocol + '//' + window.location.host,
            urlid;
        urlid = parseInt(pageid) - 1;
        if (urlid < 0)
            urlid = 5;
        $("#previous").attr("href", TheDemoURL + '/pages/service1.html?id=' + urlid)
    };

    $("#location").attr("href", "serviceinlocation.html?id=" + pageid);
    $("#staff").attr("href", "serviceoperate.html?id=" + pageid);

    $.ajax({
        type: 'GET',
        url: '../service',
        datatype: 'json',
        success: function (data) {
            findlocation();
            findstaff();

        },
        error: function (data) {
            alert(data);
        }

    });
    var locationnum;

    function findlocation() {
        $.ajax({
            type: 'GET',
            url: '../available',
            datatype: 'json',
            success: function (response) {
                var count = 0;
                for (var i = 0; i < JSON.parse(response).length; i++) {

                    if (JSON.parse(response)[i].serviceid == pageid) {
                        locationnum = JSON.parse(response)[i].locationid;
                        $.ajax({
                            type: 'GET',
                            url: '../location',
                            datatype: 'json',
                            success: function (reply) {
                                loadLocation(reply);
                            },
                            error: function (data) {
                                alert(data);
                            },
                            async: false

                        });
                        console.log(locationnum);
                        count++;
                    }


                }
                console.log(count);

            },
            error: function (response) {
                alert(data);
            }

        });
    };
    var staffnum;

    function findstaff() {
        $.ajax({
            type: 'GET',
            url: '../operation',
            datatype: 'json',
            success: function (response) {
                var count = 0;
                for (var i = 0; i < JSON.parse(response).length; i++) {

                    if (JSON.parse(response)[i].serviceid == pageid) {
                        staffnum = JSON.parse(response)[i].staffid;
                        $.ajax({
                            type: 'GET',
                            url: '../staff',
                            datatype: 'json',
                            success: function (reply) {
                                loadStaff(reply);
                            },
                            error: function (data) {
                                alert(data);
                            },
                            async: false

                        });
                        console.log(staffnum);
                        count++;
                    }


                }
                console.log(count);

            },
            error: function (response) {
                alert(data);
            }

        });
    };

    function loadLocation(json) {
        var el = "";
        el += '<div class="col-md-3 w3_agileits_services_grid hvr-overline-from-center"><div class="w3_agileits_services_grid_agile"><div class="w3_agileits_services_grid_1"><img src="' + JSON.parse(json)[locationnum].photo + '"alt="service-img"></div><h3><a href="location1.html?id=' + locationnum + '">' + JSON.parse(json)[locationnum].name + '</a></h3><p>' + JSON.parse(json)[locationnum].information + '</p></div></div>';
        $(".w3_agileits_services_grids").append(el);
    }

    function loadStaff(json) {
        var el = "";
        el += '<div class="col-md-4 ser-in"><a href="staff1.html?id=' + staffnum + '" class="b-link-stripe b-animate-go  thickbox"><div class="b-line b-line1"></div><div class="b-line b-line2"></div><div class="b-line b-line3"></div><div class="b-line b-line4"></div><div class="b-line b-line5"></div><img class="img-responsive" src="' + JSON.parse(json)[staffnum].photo + '"style="top: 0px;" ><div class="b-wrapper"><h2 class="b-animate b-from-left b-delay03 "><img src="../assets/img/plus.png" alt="" style="top: 0px;"></h2></div></a><h5>' + JSON.parse(json)[staffnum].name + '</h5><span>' + JSON.parse(json)[staffnum].position + '</span><p>' + JSON.parse(json)[staffnum].brief + '</p></div>';
        $(".service-grid").append(el);
    }




});
