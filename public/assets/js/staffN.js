$(document).ready(function () {
    var pageid = GetURLParameter('id');

    function GetURLParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }
    $.ajax({
        type: 'GET',
        url: '../staff',
        datatype: 'json',
        success: function (data) {
            $("#snumber").text(JSON.parse(data)[pageid].name);
            $("#stitle").text(JSON.parse(data)[pageid].name);
            $("#sp").text(JSON.parse(data)[parseInt(pageid)].introduction);
            $("#spic").attr("src", JSON.parse(data)[pageid].photo);
            $("#sposition").text("Position: " + JSON.parse(data)[pageid].position);
            $("#email").text(JSON.parse(data)[pageid].email);
            //js for Staff's Services
            $("#servicenum").text(JSON.parse(data)[pageid].name);
            $("#servicenum").attr("href", "staff1.html?id=" + pageid);
            $("#sitem").text(JSON.parse(data)[pageid].name + " operations");
            $("#sername").text(JSON.parse(data)[pageid].name);

            setNext();
            setPrevious();
        },
        error: function (data) {
            alert(data);
        }

    });
    setNext = function () {
        var TheDemoURL = window.location.protocol + '//' + window.location.host,
            urlid;
        urlid = parseInt(pageid) + 1;
        if (urlid > 5)
            urlid = 0;
        $("#next").attr("href", TheDemoURL + '/pages/staff1.html?id=' + urlid)
    };
    setPrevious = function () {
        var TheDemoURL = window.location.protocol + '//' + window.location.host,
            urlid;
        urlid = parseInt(pageid) - 1;
        if (urlid < 0)
            urlid = 5;
        $("#previous").attr("href", TheDemoURL + '/pages/staff1.html?id=' + urlid)
    };
    $("#operation").attr("href", "staffoperate.html?id=" + pageid);
    $.ajax({
        type: 'GET',
        url: '../staff',
        datatype: 'json',
        success: function (data) {
            findservice();


        },
        error: function (data) {
            alert(data);
        }

    });
    var servicenum;

    function findservice() {
        $.ajax({
            type: 'GET',
            url: '../operation',
            datatype: 'json',
            success: function (response) {
                var count = 0;
                for (var i = 0; i < JSON.parse(response).length; i++) {

                    if (JSON.parse(response)[i].staffid == pageid) {
                        servicenum = JSON.parse(response)[i].serviceid;
                        $.ajax({
                            type: 'GET',
                            url: '../service',
                            datatype: 'json',
                            success: function (reply) {
                                loadService(reply);
                            },
                            error: function (data) {
                                alert(data);
                            },
                            async: false

                        });
                        console.log(servicenum);
                        count++;
                    }


                }
                console.log(count);

            },
            error: function (response) {
                alert(data);
            }

        });
    };

    function loadService(json) {
        var el = "";
        el += '<div class="col-md-4 img-top "><img src="' + JSON.parse(json)[servicenum].photo + '" style="top: 0px;"><div class="fashion"><h5><a href="service1.html?id=' + servicenum + '">' + JSON.parse(json)[servicenum].name + '</a></h5><p>' + JSON.parse(json)[servicenum].introduction + '</p></div></div>';
        $(".tab_img").append(el);
    }


});
